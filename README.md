# rockpro64-utils

Utility scripts for Rockpro64 and possibly others


Usage: status.sh [-h] [-c $path] [-f] [-l] [-L] [-m] [-u] [-v]

############################################################################

 Use status.sh for the following tasks:

 status.sh -c /path/to/test performs disk health/performance tests  
 status.sh -l outputs diagnostic logs to the screen via less  
 status.sh -L outputs diagnostic logs to the screen as is  
 status.sh -m provides simple CLI monitoring  
 status.sh -n provides simple CLI network monitoring


############################################################################
